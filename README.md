# Pipeline gCubeBuild

Given a git repository and a valid branch, a maven build is performed using jdk8.
Predefined maven goal: "clean deploy". It's possible to add further build options by input parameter


## USAGE TIPS:

It can be added to a jenkinsjob or invoked manually

### Expected parameters (mandatory)

GIT_URL url of the project to build; 

GIT_BRANCH branch of the project to build.

In this first version the jdk used is jdk8. In the future we can specify the jdk as input parameter. 

