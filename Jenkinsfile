/**
 * Given a git repository and a valid branch, a maven build is performed using jdk8
 *
 * Roberto Cirillo (ISTI-CNR)
 */

pipeline {
    
    options {
        ansiColor('xterm')
    }
    tools {
       maven 'Maven 3-6-2'
       jdk 'OpenJDK 8'
    }
    environment {
        JAVA_HOME='/usr/lib/jvm/java-8-openjdk-amd64'
        GIT_URL= "${params.GIT_URL}"
        GIT_BRANCH="${params.GIT_BRANCH.split('/').size() > 1 ? params.GIT_BRANCH.split('/')[1..-1].join('/') : params.GIT_BRANCH}"
        gcube_settings="${params.gcube_settings}"
        local_repo="${params.local_repo}"
        exec_label="${params.exec_label}"
    }
    parameters {
      string(name: 'GIT_URL',
             defaultValue: '',
             description: 'git url')
      string(name: 'GIT_BRANCH',
             defaultValue: '',
             description: 'git branch')
      string(name: 'gcube_settings',
             defaultValue: 'settings.xml',
             description: 'the maven settings for gcube')
      string(name: 'local_repo',
             defaultValue: 'repository',
             description: 'The location of the local repository') 
      string(name: 'exec_label',
             defaultValue: 'CI',
             description: 'Run on all nodes matching the label')
      string(name: 'build_options',
             defaultValue: '',
             description: 'Additional options for Maven.')
      string(name: 'maven_goal',
             defaultValue: 'clean deploy',
             description: 'maven goal. Useful if deploy is not needed')
    }  
    agent
    {
        node
        {
            label "${exec_label}"
        }
    }
    stages {
        stage('Checkout git project') {
            steps {
                echo " env GIT_URL: ${env.GIT_URL}"
                echo " env GIT_BRANCH: ${env.GIT_BRANCH}"
                echo " param GIT_URL: ${params.GIT_URL}"
                echo " param GIT_BRANCH: ${params.GIT_BRANCH}"
                echo " GIT_URL: ${GIT_URL}"
                echo " GIT_BRANCH: ${GIT_BRANCH}"
                git branch: "${env.GIT_BRANCH}", credentialsId: '88b54962-1c0e-49cb-8155-22276860f346', url: "${GIT_URL}"
            }
        }
        stage('Build the job') {
            steps {
                sh 'mvn --settings $MAVEN_CONFIG_FOLDER/$gcube_settings -Dmaven.repo.local=$MAVEN_CONFIG_FOLDER/$local_repo $build_options dependency:tree $maven_goal'
            }
        }
    
        stage('Clean') {
          steps{
            sh "mvn --settings $MAVEN_CONFIG_FOLDER/$gcube_settings -Dmaven.repo.local=$MAVEN_CONFIG_FOLDER/$local_repo clean"
            sh "rm -Rf $MAVEN_CONFIG_FOLDER/../workspace/gCubeBuild/src/main/webapp/*"
          }
        }
    }
   // post-build actions
    post {
        success {
            echo 'The gCubeBuild pipeline worked!'
            emailext to: 'jenkinsbuilds@d4science.org',
                    subject: "[Jenkins gCubeBuild Pipeline] build ${currentBuild.fullDisplayName} worked",
                    body: "Build time: ${currentBuild.durationString}. See ${env.BUILD_URL}"
        }
        failure {
            echo 'The gCubeBuild pipeline has failed'
            emailext attachLog: true,
                    to: 'jenkinsbuilds@d4science.org',
                    subject: "[Jenkins DockerPipeline D4S] build ${currentBuild.fullDisplayName} failed",
                    body: "Something is wrong with ${env.BUILD_URL}"
        }
    }
}
